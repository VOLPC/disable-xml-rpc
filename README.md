# README #

To use this plugin on your Wordpress site, just download a copy of the zip file and upload it to your Wordpress installation.  Then activate the plugin.  This is an extremely simple plugin and provides no user interface.


### Need Help? ###

* Feel free to contact VOLPC Custom Computing if you need help or have questions.
* [CustomerService@volpc.com](mailto:CustomerService@volpc.com)