<?php
/**
 * Plugin Name: Disable XML-RPC
 * Version:     1.0
 * Plugin URI:   https://www.volpc.com/wordpress/disable-xml-rpc-in-wordpress/
 * Description: Disable XMLRPC server for WP >= v3.5
 * Author:      Greg Volpe
 * Author URI:   https://www.volpc.com/
 */
 
add_filter('xmlrpc_enabled', '__return_false');
 
?>